package org.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/upload")
@MultipartConfig
public class FileUploadServlet extends HttpServlet {

    private static final Logger logger = LogManager.getLogger(FileUploadServlet.class);

    private String uploadPath;

    @Override
    public void init() {
        logger.warn("Init");
        uploadPath = getUploadPathFromDatabase();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Part filePart = request.getPart("file");
        String fileName = getSubmittedFileName(filePart);
        InputStream fileContent = filePart.getInputStream();
        OutputStream out = new FileOutputStream(uploadPath + "/" + fileName);
        int read;
        final byte[] bytes = new byte[1024];
        while ((read = fileContent.read(bytes)) != -1) {
            out.write(bytes, 0, read);
        }
        out.close();
        response.setContentType("text/html");
        PrintWriter outResponse = response.getWriter();
        outResponse.println("<html><body><h2>File " + fileName + " uploaded successfully.</h2></body></html>");
    }

    private String getSubmittedFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String fileName = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1); // MSIE fix.
            }
        }
        return null;
    }

    private String getUploadPathFromDatabase() {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        String path = "";
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            // Provide database URL, username, and password
            String url = "jdbc:mariadb://localhost:3306/test";
            String user = "su";
            String password = "password1";

            // Create connection
            conn = DriverManager.getConnection(url, user, password);

            // Create statement
            stmt = conn.createStatement();

            // Execute query
            String sql = "SELECT path FROM Path WHERE id = 3;";
            rs = stmt.executeQuery(sql);

            // Process the result set
            while (rs.next()) {
                path = rs.getString("path");

            }
        } catch (SQLException|ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            // Close resources
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
                if (conn != null) conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        logger.warn("Got path: " + path);
        return path;
    }
}
